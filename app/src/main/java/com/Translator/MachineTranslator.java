package com.Translator;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Lilium Aikia on 11/1/2015.
 */
public class MachineTranslator
{
    private static MachineTranslator _singleton = null;
    public static MachineTranslator getInstance(XmlPullParser res)
    {
        if(_singleton == null)
        {
            _singleton = new MachineTranslator(res);
        }

        return _singleton;
    }

    public static MachineTranslator getInstance()
    {
        return _singleton;
    }

    private Hashtable<String,Word> _dictionary;
    private String _debugEnable;

    public void LoadDictionary(XmlPullParser xpp)
    {
        _dictionary = new Hashtable<>();

        if(xpp == null)
        {
            return;
        }

        try
        {
            Word w = null;

            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT)
            {
                if (xpp.getEventType()==XmlPullParser.START_TAG)
                {
                    if (xpp.getName().equals("Word"))
                    {
                        w = new Word();
                        w.Destinations = new ArrayList<>();
                    }
                    else
                    {
                        if(xpp.getName().equals("Source"))
                        {
                            xpp.next();
                            w.Source = xpp.getText();
                        }
                        else
                        {
                            if(xpp.getName().equals("Destination"))
                            {
                                w.Size++;
                                xpp.next();
                                w.Destinations.add(xpp.getText());
                            }
                        }
                    }
                }
                else
                {
                    if (xpp.getEventType()==XmlPullParser.END_TAG)
                    {
                        _dictionary.put(w.Source.toLowerCase(), w);
                    }
                }

                xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MachineTranslator(XmlPullParser res)
    {
        _debugEnable = "";

        try
        {
            LoadDictionary(res);
//            URL url = new URL("android.resource://engenoid.tessocrtest.translator/raw/dictionary");
//            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//            DocumentBuilder db = dbf.newDocumentBuilder();
//            Document doc = db.parse(new InputSource(url.openStream()));
//            doc.getDocumentElement().normalize();
//
//            NodeList nodeList = doc.getElementsByTagName("Word");
//
//            _dictionary = new Hashtable<>(nodeList.getLength());
//
//            for (int i = 0; i < nodeList.getLength(); i++)
//            {
//
//                Node node = nodeList.item(i);
//                Word w = new Word();
//
//                Element fstElmnt = (Element) node;
//                NodeList sourceList = fstElmnt.getElementsByTagName("Source");
//                Element sourceElement = (Element) sourceList.item(0);
//                sourceList = sourceElement.getChildNodes();
//                w.Source = ((Node) sourceList.item(0)).getNodeValue();
//
//                NodeList destinationList = fstElmnt.getElementsByTagName("Destination");
//                w.Size = destinationList.getLength();
//                w.Destinations = new String[w.Size];
//
//                for (int j = 0; j < destinationList.getLength(); j++) {
//                    Element destinationElement = (Element) destinationList.item(0);
//                    NodeList destinationValue = destinationElement.getChildNodes();
//                    w.Destinations[j] = ((Node) destinationValue.item(0)).getNodeValue();
//                }
//
//                _dictionary.put(w.Source, w);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void DebugEnable(String tag)
    {
        _debugEnable = tag;
    }

    public void DebugDisable()
    {
        _debugEnable = "";
    }

    public void WriteLog(String message)
    {
        if(_debugEnable.equals("") == false)
        {
            Log.d(_debugEnable, message);
        }
    }

    public String TranslateWord(String word)
    {
        String result = " ";

        if(word.equals("") == false)
        {
            String editedWord = word.replace(".","");
            editedWord = editedWord.replace(",","");
            editedWord = editedWord.toLowerCase();

            //result = String.valueOf(_dictionary.size());

            Word w = _dictionary.get(editedWord);
            if(w != null)
            {
                if(w.Destinations != null)
                {
                    if(w.Destinations.size() > 0)
                    {
                        result = w.Destinations.get(0);
                    }
                }
            }
            else
            {
                result = word;
            }
        }

        return editResultWord(result);
    }

    private String editResultWord(String result) {

        StringBuilder re = new StringBuilder();
        for(int i = 0 ; i < result.length(); i++)
        {
            char c = result.charAt(i);
            if(c == ',' || c == ';' || c=='|' || c=='(' || c=='/' ){
                break;
            }
            re.append(c);
        }
        return re.toString();
    }

    public String Translate(String source, String language)
    {
        String result = "";

        if(language == "English"
                && source.equals("") == false)
        {
            String[] separated = source.split(" ");
            for(int i=0; i<separated.length; i++)
            {
                result = result +  " " + TranslateWord(separated[i]);
            }

            WriteLog("Translate Success: Words=" + separated.length + " !");
        }
        else
        {
            WriteLog("Can't translate!");
        }

        return result;
    }
}
