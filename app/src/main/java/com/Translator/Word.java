package com.Translator;

import java.util.List;

/**
 * Created by Lilium Aikia on 11/1/2015.
 */
public class Word
{
    public String Source;
    public List<String> Destinations;
    public int Size;
}
