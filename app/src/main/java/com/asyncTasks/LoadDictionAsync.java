package com.asyncTasks;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;

import com.Translator.MachineTranslator;
import com.renard.ocr.R;

import org.xmlpull.v1.XmlPullParser;

/**
 * Created by LeeSan on 12/4/2015.
 */
public class LoadDictionAsync extends AsyncTask<Void, Void, Void> {
    private Context m_acctivity;
    // Hàm chạy đầu tiên của tiểu trình
    public LoadDictionAsync (Context acc){
        this.m_acctivity = acc;

    }
    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();

    }

    @Override
    protected Void doInBackground(Void... arg0) {

        initTranslator();
        return  null;

    }
    private void initTranslator() {

        Resources res = m_acctivity.getResources();
        XmlPullParser xrp = res.getXml(R.xml.dictionary);
        MachineTranslator.getInstance(xrp);
    }
    @Override
    protected void onProgressUpdate(Void... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);



    }

    // Hàm chạy cuối cùng của tiểu trình
    @Override
    protected void onPostExecute(Void result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);

    }
}
